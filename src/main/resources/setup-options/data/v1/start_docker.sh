#!/bin/sh
#DOCK_PORT=$(sudo cat /opt/jbox/work/*jira*/conf/server.xml | grep "Connector port" | head -1 | sed 's/.*=\"//' | sed 's/\"//')
DOCK_IP=$(sudo /sbin/ifconfig eth1 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
sudo docker run -d -P=true -p=$DOCK_PORT:8080 -v /opt/jbox/work/:/opt/jbox/work -t cnortje/java_base_v1 /bin/sh /opt/jbox/work/docker_start_jira.sh
