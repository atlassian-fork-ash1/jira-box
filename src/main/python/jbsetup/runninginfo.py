""" Collection of methods to manage running dockers in vagrant """
__author__ = 'jsimon'
from jbsetup import util
import os
    
INFO_PATH = '/opt/jbox/script/jira-box/.jirabox'
 

def add_running_instance(setup_info, docker_hash, port):
    """ Store information about running instance """
    current_info = util.load_json_file(INFO_PATH) if os.path.exists(INFO_PATH) else []

    new_info = _format_info(setup_info, docker_hash, port)
    current_info.append(new_info)
    util.dump_json_file(current_info, INFO_PATH)


def remove(n):
    """ Remove information about running instance """
    current_info = util.load_json_file(INFO_PATH)
    del current_info[n]
    util.dump_json_file(current_info, INFO_PATH)
    

def _format_info(setup_info, docker_hash, port):
    info = {'DOCKER': docker_hash, 'PORT': port}
    info['JIRA_VERSION'] = setup_info['JIRA_VERSION'].split('/')[-1].replace('jira-standalone-distribution-',
                                                                             '').replace('-standalone.tar.gz', '')
    info['DB'] = setup_info['DB_TYPE']
    info['JAVA_VERSION'] = setup_info['JAVA_VERSION']
    return info


def print_info():
    """ Shors information about running instance in a table"""
    from tabulate import tabulate
    current_info = util.load_json_file(INFO_PATH) if os.path.exists(INFO_PATH) else []
    tabulated_info = []
    for i, info in enumerate(current_info):
        tabulated_info.append([i, info['JIRA_VERSION'], info['JAVA_VERSION'], info['DB'], info['DOCKER'], info['PORT']])
    print tabulate(tabulated_info, headers=['JIRA v', 'Java v', 'DB', 'DOCKER', 'PORT'])


def get_used_ports():
    current_info = util.load_json_file(INFO_PATH) if os.path.exists(INFO_PATH) else []
    return [x['PORT'] for x in current_info]


def get_docker_hash(i):
    """ Gets the docker hash of the ith element in the info list """
    current_info = util.load_json_file(INFO_PATH) if os.path.exists(INFO_PATH) else []
    return current_info[i]['DOCKER']
