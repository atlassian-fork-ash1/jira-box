__author__ = 'jsimon'

import json
import os
import zipfile
import shutil
import tarfile
import socket
import fcntl
import struct
from subprocess import Popen
from subprocess import PIPE
from tempfile import mkstemp


def query_config(question, valid_values, allow_custom=False):
    """Ask a a question via raw_input() and return their answer.
    Params:
        "question" is a string that is presented to the user.
        "valid_values" is the list of accepted values
    Returns: The "answer" return value.
    """

    print question
    for i, v in enumerate(valid_values):
        print " %s) %s" % (i + 1, v)
    while True:
        raw_val = None
        try:
            raw_val = raw_input().lower()
            choice = int(raw_val)
            return valid_values[choice-1], False
        except (IndexError, ValueError):
            if allow_custom:
                return raw_val, True
            else:
                print 'Please enter a valid number'


def replace(file_path, pattern, subst):
    """ Replaces pattern with subst in given file path """
    # Create temp file
    fh, abs_path = mkstemp()
    new_file = open(abs_path,'w')
    old_file = open(file_path)
    for line in old_file:
        new_file.write(line.replace(pattern, subst))
    # close temp file
    new_file.close()
    os.close(fh)
    old_file.close()
    # Remove original file
    os.remove(file_path)
    # Move new file
    shutil.move(abs_path, file_path)


def get_resource_path(resource, version='v1'):
    """ Gets path for given resource """
    resource_path = os.path.join(str(os.getcwd()).replace("/python", ""), 'resources/setup-options/data', version, resource)
    return resource_path


def download(download_url, dest_folder):
    """ Downloads given url in given directory unless it's already there """
    file_name = download_url.split('/')[-1]
    download_path = os.path.join(dest_folder, file_name)
    if not os.path.exists(download_path):
        if download_url.startswith('http'):
            args = ['wget', download_url, '-P', dest_folder]
            output = Popen(args, stdout=PIPE)
            print output
        else:  # local file
            path = get_resource_path(download_url)
            shutil.copy(path, download_path)
    return download_path
    

def extract(compressed_file_path, dest_folder):
    """ Extracts compressed file """
    ext = os.path.splitext(compressed_file_path)[1]
    functions = {'.zip': unzip,
                 '.gz': untar}
    return functions[ext](compressed_file_path, dest_folder)


def unzip(zip_file_path, dest_folder):
    """
    Extracts provided zip in dest_path. Overwrites existing.
    Assumes zip file contains just a single folder in top level
    
    Returns: the path to the extracted folder
    """
    with zipfile.ZipFile(zip_file_path, 'r') as zip_file:
        folder_name = zip_file.namelist()[0]
        dest_path = os.path.join(dest_folder, folder_name)
        if os.path.exists(dest_path):
            shutil.rmtree(dest_path)
        zip_file.extractall(dest_folder)
        return dest_path


def untar(tar_file_path, dest_folder):
    """
    Extracts provided tar.gz in dest_path. Overwrites existing.
    Assumes tar.gz file contains just a single folder in top level
    
    Returns: the path to the extracted folder
    """
    with tarfile.open(tar_file_path, 'r') as tar_file:
        folder_name = os.path.commonprefix(tar_file.getnames())
        dest_path = os.path.join(dest_folder, folder_name)
        if os.path.exists(dest_path):
            shutil.rmtree(dest_path)
        tar_file.extractall(dest_folder)
        return dest_path


def load_file(filename):
    """ Loads a file in memory and returns it's contents"""
    path = os.path.join(os.getcwd(), get_resource_path(filename))
    with open(path, 'r') as f:
        return f.read()


def load_json_file(filename):
    return json.loads(load_file(filename))


def dump_json_file(contents, file_path):
    with open(file_path, 'w+') as fp:
        json.dump(contents, fp)


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])